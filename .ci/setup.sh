#!/usr/bin/env bash
set -euxo pipefail
if [[ -d pacman ]]; then
    echo "Restoring pacman cache"
    rm -rf /var/cache/pacman
    mv pacman /var/cache/
    ls -alh /var/cache/pacman/pkg
fi
curl -Lo /etc/pacman.d/mirrorlist "https://archlinux.org/mirrorlist/?country=DE&protocol=https&use_mirror_status=on"
sed -i 's/^#//' /etc/pacman.d/mirrorlist

pacman-key --init
pacman-key --add keys/therisen.pub
pacman-key --lsign-key "$SIGNINGKEYID"

if [[ "${FORCE_REBUILD:-false}" != "true" ]]; then
    cat <<'EOF' >>/etc/pacman.conf
[therisen]
Server = https://arch-therisen06-5854c582a70f80c079e7310f5a0447c6ae463b29fc039b7.gitlab.io/$repo/$arch
EOF
fi

pacman -Syu --noconfirm sudo just
useradd --create-home builder
echo 'Defaults env_keep += "ftp_proxy http_proxy https_proxy no_proxy FTP_PROXY HTTP_PROXY HTTPS_PROXY NO_PROXY"' >>/etc/sudoers
echo 'builder ALL = NOPASSWD: /usr/bin/pacman' >/etc/sudoers.d/builder-pacman
echo 'Packager="Arch CI Builder <therisen06@gmail.com>"' >>/etc/makepkg.conf
install -d -m 0700 -o builder /home/builder/.gnupg
sudo -u builder gpg --import keys/*.pub
