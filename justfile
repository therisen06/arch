_default:
    just -l

# Update all aur package definitions
update-aur:
    #!/usr/bin/env bash
    set -euo pipefail
    find pkgs/aur -maxdepth 1 -mindepth 1 -type d | while read -r pkg; do
        pkgname="$(basename $pkg)"
        echo ">> Updating $pkgname"
        git subtree pull --prefix "$pkg" "https://aur.archlinux.org/$pkgname.git" master --squash
    done

# Import a package from the AUR
import-aur PKG:
    git subtree add --prefix "pkgs/aur/{{ PKG }}" "https://aur.archlinux.org/{{ PKG }}.git" master --squash

# Build all packages
build-all:
    #!/usr/bin/env bash
    set -euo pipefail
    find pkgs -name PKGBUILD -type f | while read fname; do
        pkg="$(dirname $fname)"
        scripts/build-or-download.sh "$pkg"
    done

# Build the provided package PKG
build PKG:
    scripts/build-or-download.sh {{ PKG }}
