#!/usr/bin/env bash
SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
set -euo pipefail

PKG=$1
echo "*******************************************************************************
Processing package: $PKG
*******************************************************************************"

UPSTREAM_VERSION="n/a"
PKGNAME=$(grep '^pkgname=' "$PKG/PKGBUILD" | cut -d= -f2)
# strip single quotes
PKGNAME=${PKGNAME//\'/}
# strip quotes
PKGNAME=${PKGNAME//\"/}

if pacman -Si "therisen/$PKGNAME" >/dev/null; then
    UPSTREAM_VERSION=$(pacman -Si "therisen/$PKGNAME" | grep "^Version" | awk -F: '{ print $2; };' | sed -e 's/\s*//g')
fi

MY_VERSION=$(sh -c ". $PKG/PKGBUILD && echo \$pkgver-\$pkgrel")
MY_VERSION="${MY_VERSION#*=}"

echo "[DEBUG] $PKG: MY_VERSION=$MY_VERSION, UPSTREAM_VERSION=$UPSTREAM_VERSION"

if [[ "$UPSTREAM_VERSION" = "$MY_VERSION" ]]; then
    "$SCRIPT_DIR"/download.sh "$PKG" || {
        echo "Download failed, building $PKG instead"
        "$SCRIPT_DIR"/build.sh "$PKG"
    }
else
    "$SCRIPT_DIR"/build.sh "$PKG"
fi
