#!/usr/bin/env bash
set -euo pipefail

PKG=$1
PKGNAME=$(grep pkgname= "$PKG/PKGBUILD" | cut -d= -f2)
echo ">> $PKGNAME: fetching previously built package"
sudo pacman -S --downloadonly --noconfirm "$PKGNAME"
mkdir -p out
find /var/cache/pacman/pkg -name "$PKGNAME-*.pkg.tar.zst" | while read -r fname; do
    cp -v "$fname" out/
done
