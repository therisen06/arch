#!/usr/bin/env bash
set -euo pipefail

PKG=$1
echo ">> $PKG: building"
sudo pacman -Sy --noconfirm base-devel namcap just
mkdir -p /tmp/build
BUILDDIR="$(mktemp -p /tmp/build -d)"

cd "$PKG"
cp -a . "$BUILDDIR/"
cd "$BUILDDIR"
echo "Running makepkg"
makepkg --syncdeps --noconfirm --log
echo "makepkg successful"
namcap ./*.tar.zst || true
