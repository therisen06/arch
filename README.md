[![pipeline status](https://gitlab.com/therisen06/arch/badges/main/pipeline.svg)](https://gitlab.com/therisen06/arch/-/commits/main)

# Arch

Some prebuilt Arch Linux packages which I use.
The packages are available [here](https://arch-therisen06-5854c582a70f80c079e7310f5a0447c6ae463b29fc039b7.gitlab.io/).

## Getting Started

Import public signing key:

```
# curl -Ls https://gitlab.com/therisen06/arch/-/raw/main/keys/therisen.pub | pacman-key --add -
# pacman-key --lsign-key C6A319A835574EF9B0AE47AAB0A683A145A70DED
```

Add this to your `pacman.conf`:

```
[therisen]
Server = https://arch-therisen06-5854c582a70f80c079e7310f5a0447c6ae463b29fc039b7.gitlab.io/$repo/$arch
```

Run `pacman -Sy`. Now you can install some packages from my repo.

## Operations

We use [git subtrees](https://www.atlassian.com/git/tutorials/git-subtree) instead of git submodules in order to easily patch files.

### Generate Signing Key

Only necessary once the existing one has expired.

1. Generate new keypair:

```bash
gpg --full-generate-key
KEYID=...
gpg --export-secret-keys --armor $KEYID > therisen.key
gpg --export --armor $KEYID > keys/therisen.pub
```

2. Upload the private key as a [Secure File](https://gitlab.com/help/ci/secure_files/index) in Gitlab.
3. Trigger a new CI pipeline and set the variable `FORCE_REBUILD` to `true`.
